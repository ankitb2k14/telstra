import { FETCH_PRODUCTS, ORIGINAL_PRODUCTS, REMOVE_PRODUCT_FROM_SCREEN, ADD_PRODUCT_TO_SCREEN } from '../../constants/constants';
import products from '../../data/products.json';

/**
 * fetch product lists from database
 * @author Ankit
 * @param null
 */
export const fetchProducts = () => (dispatch) => {
    dispatch({
        type: FETCH_PRODUCTS,
        payload: products.filter((item) => {
            return item.isPublished === "true"
          })
    });
    dispatch({
        type: ORIGINAL_PRODUCTS,
        payload: products.filter((item) => {
            return item.isPublished === "true"
          })
    });
}

/**
 * remove products from main screen 
 * @author Ankit
 * @param {number}
 */
export const removeProductFromScreen = (index) => (dispatch) => {
    dispatch({
        type: REMOVE_PRODUCT_FROM_SCREEN,
        payload: index
    });
}

/**
 * add product from main screen 
 * @author Ankit
 * @param {object}
 */
export const addProductToScreen = (product) => (dispatch) => {
    dispatch({
        type: ADD_PRODUCT_TO_SCREEN,
        payload: product
    });
};
