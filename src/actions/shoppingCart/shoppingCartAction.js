import { ADD_PRODUCT_TO_CART, REMVOE_PRODUCT_FROM_CART } from '../../constants/constants';

/**
 * add products to shopping cart
 * @author Ankit
 * @param {object}
 */
export const addProductToCart = (product) => (dispatch) => {
    dispatch({
        type: ADD_PRODUCT_TO_CART,
        payload: product
    });
}

/**
 * remove product from shopping cart 
 * @author Ankit
 * @param {number}
 */
export const removeProductFromCart = (index) => (dispatch) => {
    dispatch({
        type: REMVOE_PRODUCT_FROM_CART,
        payload: index
    });
}
