import React, { Component } from 'react';
import PropTypes from "prop-types";
import { AppBar, Toolbar, Badge, IconButton, Typography, Hidden, Button } from '@material-ui/core';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -18,
        marginRight: 10
    },
    badge: {
        top: '50%',
        right: -3,
        cursor: 'default',
        // The border color match the background color.
        border: `2px solid ${
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900]
        },`
    },
    btnColor: {
        color: 'white',
    }
});

class HeaderBar extends Component {
    constructor(props){
        super(props);

        this.state = {
            showBackBtn: false
        }
    }

    //Custom Functions
    
    /**
     * Show Shopping Cart
     * @author: Ankit
     * @param: null
     * @return: null
   */
    openShoppingCart = () => {
        this.setState(() => ({ showBackBtn: true }), () => { this.props.openShoppingCart(); });
    };

    /**
     * Back to Main Screen
     * @author: Ankit
     * @param: null
     * @return: null
   */
    backToMainScreen = () => {
        this.setState(() => ({ showBackBtn: false }), () => { this.props.backToMainScreen(); });
    }
    render() {
        const { classes, ShoppingCartArr } = this.props;
        const { showBackBtn } = this.state;
        return (
            <div className={classes.root}>
                <AppBar position="static" >
                    <Toolbar variant="dense">
                        <Hidden smUp>
                            { showBackBtn ? 
                            <Button size="small" className={classes.btnColor} onClick={this.backToMainScreen}>
                                <KeyboardArrowLeft />
                                Back
                            </Button>
                            : null }
                            <Typography className={classes.grow}></Typography>
                            <IconButton aria-label="Cart" onClick={this.openShoppingCart} >
                                <Badge badgeContent={ShoppingCartArr.length} color="secondary" classes={{ badge: classes.badge }} >
                                    <ShoppingCartIcon />
                                </Badge>
                            </IconButton>
                        </Hidden>
                    </Toolbar>
                </AppBar>
            </div >
        );
    }
}

HeaderBar.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(HeaderBar);
