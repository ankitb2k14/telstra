import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardActions, CardContent, CardMedia, Grid, Typography, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%',
        backgroundSize: 'contain'
    },
    cardContent: {
        flexGrow: 1,
    },
    button: {
        margin: theme.spacing.unit,
        borderRadius: '20px'
    },
    cardActions: {
        justifyContent: 'center'
    }
});


function ProductList(props) {
    const { classes, productItem, index } = props;
    return (
        <Grid item xs={12} sm={6} md={4} lg={4}>
            <Card className={classes.card}>
                <CardContent className={classes.cardContent}>
                    <Typography gutterBottom align="center" variant="title" component="h5">
                        {productItem.productName}
                    </Typography>
                </CardContent>
                <CardMedia
                    className={classes.cardMedia}
                    image={productItem.productImage}
                    title="Image title"
                />
                <CardContent className={classes.cardContent}>
                    <Typography align="center" variant="title" component="h5">
                        ${productItem.price}
                    </Typography>
                </CardContent>
                <CardActions className={classes.cardActions}>
                    <Button variant="contained" color="primary" className={classes.button} onClick={() => props.addProductToCart(productItem, index)}>
                        Add to Card
                     </Button>
                </CardActions>
            </Card>
        </Grid>
    );
}

ProductList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductList);