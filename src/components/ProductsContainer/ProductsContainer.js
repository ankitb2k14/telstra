import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CssBaseline, Hidden, Grid, Typography } from '@material-ui/core';
import { ProductList, ShoppingCart, HeaderBar } from '..';
import { withStyles } from '@material-ui/core/styles';
import { fetchProducts, removeProductFromScreen, addProductToScreen } from '../../actions/products/productAction';
import { addProductToCart, removeProductFromCart } from '../../actions/shoppingCart/shoppingCartAction';
import { connect } from 'react-redux'

const styles = theme => ({
  layout: {
    width: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },

  gridContainer: {
    width: '70%',
    margin: 'auto'
  },
  shopContainer: {
    width: '30%',
    margin: '20px',
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    },
  }
});



class ProductsContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      productsMobileView: false
    }
  }

  //LifeCycles
  componentDidMount(){
    this.props.fetchProducts()
  }


  //Custom Functions

  /**
   * Add Product to Cart
   * @author: Ankit
   * @param: {object, number}
   * @return: null
   */
  addProductToCart = (product, index) => {
    const { originalProducts, addProductToCart, removeProductFromScreen } = this.props;
    const originalIndex = originalProducts.findIndex((item) => {
      return item.productName === product.productName;
    });
    addProductToCart({ ...product, positionOnKey: originalIndex });
    removeProductFromScreen(index);
  }

  /**
   * Remove Product to Cart
   * @author: Ankit
   * @param: {object, number}
   * @return: null
   */
  removeProduct = (product, index) => {
    const { removeProductFromCart, addProductToScreen } = this.props;
    removeProductFromCart(index);
    addProductToScreen(product);
  };

  /**
   * Show Shopping Cart for small screen devices
   * @author: Ankit
   * @param: null
   * @return: null
   */
  openShoppingCart = () => {
    this.setState(() => ({ productsMobileView: true }));
  };

  /**
  * Back to Shopping Cart for small screen devices
  * @author: Ankit
  * @param: null
  * @return: null
  */
  backToMainScreen = () => {
    this.setState(() => ({ productsMobileView: false }));
  }

  render() {
    const { classes, products, ShoppingCartArr: SCArr}  = this.props;
    const { ShoppingCartArr } = SCArr;
    const { productsMobileView } = this.state;

    return (
      <React.Fragment>
        <CssBaseline />
        <HeaderBar ShoppingCartArr={ShoppingCartArr} openShoppingCart={this.openShoppingCart} backToMainScreen={this.backToMainScreen} />
        <main>
          <div className={classNames(classes.layout, classes.cardGrid)}>
            <Hidden xsDown={productsMobileView ? true : false} >
              <Grid item container spacing={40} className={classes.gridContainer}>
                {products.length > 0 ?
                  products.map((productItem, index) => {
                    return <ProductList productItem={productItem} key={index} addProductToCart={this.addProductToCart} index={index} />
                  }) : <Typography gutterBottom align="center" variant="title" component="h5">
                    No items are remaining!
                    </Typography>}
              </Grid>
            </Hidden>
            <Hidden xsDown={!productsMobileView ? true : false} >
              <Grid className={classes.shopContainer}>
                <ShoppingCart ShoppingCartArr={ShoppingCartArr} removeProduct={this.removeProduct} />
              </Grid>
            </Hidden>
          </div>
        </main>
      </React.Fragment>
    );
  }
}

ProductsContainer.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  products: state.products.products,
  originalProducts: state.products.originalProducts,
  ShoppingCartArr: state.ShoppingCart
});

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProducts: () => {
      dispatch(fetchProducts())
    },
    addProductToCart: (product) => {
      dispatch(addProductToCart(product))
    },
    removeProductFromScreen: (index) => {
      dispatch(removeProductFromScreen(index))
    },
    removeProductFromCart: (index) => {
      dispatch(removeProductFromCart(index))
    },
    addProductToScreen: (product) => {
      dispatch(addProductToScreen(product))
    }
  }
}

const ConnectedProductsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsContainer)

export default withStyles(styles)(ConnectedProductsContainer);