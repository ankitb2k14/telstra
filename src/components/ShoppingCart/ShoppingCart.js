import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader, Badge, IconButton, Grid } from '@material-ui/core';
import { ShoppingListItem } from '..';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    cardHeader: {
        backgroundColor: theme.palette.grey[200],
    },
    cardSubHeader: {
        fontSize: '1.4em',
        [theme.breakpoints.down('sm')]: {
            fontSize: '1.1em'
        },
    },
    cardPricing: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing.unit * 2,
    },
    cardActions: {
        [theme.breakpoints.up('sm')]: {
            paddingBottom: theme.spacing.unit * 2,
        },
    },
    container: {
        width: '90%',
        margin: '20px auto'
    },
    badge: {
        top: '50%',
        right: -3,
        cursor: 'default',
        // The border color match the background color.
        border: `2px solid ${
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900]
            }`
    }
});

const EMPTY_CART = "Your Cart is empty!"

class ShoppingCart extends Component {
    render() {
        const { classes, ShoppingCartArr } = this.props;
        return (
            <Grid item md={12} lg={12}>
                <Card>
                    <CardHeader
                        subheader={<React.Fragment>
                            <span align="center" className={classes.cardSubHeader}>
                                Shopping cart
                            </span>
                            <IconButton align="right" aria-label="Cart">
                                <Badge badgeContent={ShoppingCartArr.length} color="primary" classes={{ badge: classes.badge }}>
                                    <ShoppingCartIcon />
                                </Badge>
                            </IconButton>
                        </React.Fragment>}
                        titleTypographyProps={{ align: 'center' }}
                        subheaderTypographyProps={{ align: 'center' }}
                        className={classes.cardHeader}

                    />

                    <div className={classes.container}>
                        {ShoppingCartArr.length > 0 ?
                            ShoppingCartArr.map((product, index) => {
                                return <ShoppingListItem product={product} key={product.productName} removeProduct={this.props.removeProduct} index={index} />
                            }) : <div align="center">{EMPTY_CART}</div>}
                    </div>
                </Card>
            </Grid>
        );
    }
}

ShoppingCart.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ShoppingCart);
