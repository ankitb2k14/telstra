import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Paper, Typography, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    button: {
        borderRadius: '20px',
        [theme.breakpoints.down('sm')]: {
            fontSize: '.5em'
        },
    },
    paper: {
        padding: (theme.spacing.unit * 2) - 6,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '15px',

    },
    typography: {
        [theme.breakpoints.down('sm')]: {
            fontSize: '.74em'
        }
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

class ShoppingListItem extends Component {
    render() {
        const { classes, product, index } = this.props;
        return (
            <Paper className={classes.paper}>
                <Typography variant="subtitle1" className={classes.typography} color="textSecondary">
                    {product.productName}
                </Typography>
                <Button size="medium" variant="contained" color="secondary" className={classes.button} onClick={() => this.props.removeProduct(product, index)}>
                    Remove 
                </Button>
            </Paper>
        );
    }
}

ShoppingListItem.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ShoppingListItem);
