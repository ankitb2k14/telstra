import { combineReducers } from 'redux';
import productReducer from './products/productReducer';
import ShoppingCartReducer from './shoppingCart/shoppingCartReducer';

export default combineReducers({
    products: productReducer,
    ShoppingCart: ShoppingCartReducer
})