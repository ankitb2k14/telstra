import { FETCH_PRODUCTS, ORIGINAL_PRODUCTS, REMOVE_PRODUCT_FROM_SCREEN, ADD_PRODUCT_TO_SCREEN } from '../../constants/constants';

/**
 * Store Initial State
 */
const initialState = {
    products: [],
    originalProducts: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }
        case ORIGINAL_PRODUCTS:
            return {
                ...state,
                originalProducts: action.payload
            }
        case REMOVE_PRODUCT_FROM_SCREEN:
            state.products.splice(action.payload, 1)
            return {
                ...state,
                products: state.products
            }
        case ADD_PRODUCT_TO_SCREEN:
            state.products.splice(action.payload.positionOnKey, 0, action.payload);
            return {
                ...state,
                products: state.products
            }
        default:
            return state;
    }
}