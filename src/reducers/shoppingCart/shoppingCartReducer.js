import { ADD_PRODUCT_TO_CART, REMVOE_PRODUCT_FROM_CART } from '../../constants/constants';

/**
 * Store Initial State
 */
const initialState = {
    ShoppingCartArr: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_PRODUCT_TO_CART:
            state.ShoppingCartArr.push(action.payload);
            return {
                ...state,
                ShoppingCartArr: state.ShoppingCartArr
            }
        case REMVOE_PRODUCT_FROM_CART: 
            state.ShoppingCartArr.splice(action.payload, 1)
            return {
                ...state,
                ShoppingCartArr: state.ShoppingCartArr
            }
        default:
            return state;
    }
}